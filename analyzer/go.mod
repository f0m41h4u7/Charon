module github.com/f0m41h4u7/Charon/analyzer

go 1.14

require (
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/drewlanenga/govector v0.0.0-20160727150047-f69e9f02317e // indirect
	github.com/lytics/anomalyzer v0.0.0-20151102000650-13cee1061701
	github.com/prometheus/client_golang v1.5.1
	github.com/prometheus/common v0.9.1
)
